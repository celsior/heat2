#include <iostream>
#include <armadillo>
#include <gmm/gmm.h>

#include <math.h>

using std::cin;
using std::cout;
using std::cerr;

using namespace arma;

const double PInf = 1e25;
const double ZInf = 1e-25;


const double len = 1;
const int x_steps = 800;
const double dx = len/x_steps;
const double V = dx*dx*dx;
const double Sx = dx*dx;
double dt = 1e-4;

vec::fixed<x_steps> T;
vec::fixed<x_steps> Tn;
vec::fixed<x_steps> Tprev;

vec::fixed<x_steps> B;

double tol = 1e-8;

#include "params.h"

double sqr(double v)
{
  return v*v;
}

double sign(double v)
{
  return (v > 0 ? 1 : (v < 0 ? -1 : 0));
}

double dB(int x, double T)
{
  double K = 1e8*exp(-1e4/T);
  return (1-B(x))*(1-exp(-K*dt));
}


double Lam(int x)
{
  double b = B[x] + dB(x,(T(x)+Tn(x))/2)/2;
  double lam1 = 10 + lamT2*sqr(Tprev(x)-273);
  double lam2 = 100 + lamT2*sqr(Tprev(x)-273);

  return 1.0 / (b/lam1 + (1-b)/lam2);
}

double Dx(int x)
{
  return Sx * 1.0 / (dx * 0.5 / Lam(x)
                     + (dx * 0.5 / Lam(x+1)));
}

double CRhoV(int x)
{
  double b = B[x] + dB(x,(T(x)+Tn(x))/2)/2;
  double rho = 1700;
  double c1 = 100;
  double c2 = 1000 + cT*(Tn(x)-273);
  return (b*c1 + (1-b)*c2)*rho*V;
}

double CRhoV_prev(int x)
{
  double b = B[x];
  double rho = 1700;
  double c1 = 100;
  double c2 = 1000 + cT*(T(x)-273);
  return (b*c1 + (1-b)*c2)*rho*V;
}


double QV(int x, double T)
{
  return Q*dB(x,T)*V;
}

double Fi(int i)
{
  if (i == 0)
    return (Tn(1)-Tn(0))/dx;
  if (i == x_steps-1)
    return (Tn(x_steps-1)-Tn(x_steps-2))/dx;

  if (i == 1 || i == x_steps-2)
    return
      (CRhoV(i)*Tn(i) - CRhoV_prev(i)*T(i))/dt
      - ( Dx(i-1)*Tn(i-1) - (Dx(i-1)+Dx(i))*Tn(i) + Dx(i)*Tn(i+1) )
      - QV(i, (T(i)+Tn(i))/2);

  return
    (CRhoV(i)*Tn(i) - CRhoV_prev(i)*T(i))/dt
    - ( Dx(i-1)*Tn(i-1) - (Dx(i-1)+Dx(i))*Tn(i) + Dx(i)*Tn(i+1) )
    + V*( Dx(i)*Dx(i+1)/CRhoV(i+1)*Tn(i+2)
          - (Dx(i)*Dx(i+1)/CRhoV(i+1) + sqr(Dx(i))/CRhoV(i+1) + sqr(Dx(i))/CRhoV(i) + Dx(i)*Dx(i-1)/CRhoV(i))*Tn(i+1)
          + (sqr(Dx(i))/CRhoV(i+1) + sqr(Dx(i))/CRhoV(i) + 2*Dx(i)*Dx(i-1)/CRhoV(i) + sqr(Dx(i-1))/CRhoV(i) + sqr(Dx(i-1))/CRhoV(i-1))*Tn(i)
          - (Dx(i-1)*Dx(i)/CRhoV(i) + sqr(Dx(i-1))/CRhoV(i) + sqr(Dx(i-1))/CRhoV(i-1) + Dx(i-1)*Dx(i-2)/CRhoV(i-1))*Tn(i-1)
          + Dx(i-1)*Dx(i-2)/CRhoV(i-1)*Tn(i-2)
        )*dt/2
    - QV(i, (T(i)+Tn(i))/2);
}


double dT = 1e-4;

double dFi(int i, int j, double F)
{
  Tn(j) += dT;
  double ret = Fi(i);
  Tn(j) -= dT;
  return (ret - F) / dT;
}

gmm::dense_matrix<double> M(x_steps,x_steps);
std::vector<double> f(x_steps);
std::vector<double> u(x_steps);
gmm::csc_matrix<double> M2;

void step_gmm(double t)
{
  const size_t N = x_steps-1;

  const double d = 0;
  if (t < 1)
    {
      M(0,0) = 1;
      f[0] = 0;
    }
  else
    {
      double F = Fi(0);

      M(0,0) = dFi(0, 0, F);
      M(0,1) = dFi(0, 1, F);

      f[0] = -F;
    }

  for (size_t i = 1; i < N; ++i)
    {
      double F = Fi(i);
      if (i > 1)
        M(i,i-2) = dFi(i, i-2, F);
      M(i,i-1) = dFi(i, i-1, F);
      M(i,i) = dFi(i, i, F);
      M(i,i+1) = dFi(i, i+1, F);
      if (i < N-1)
        M(i,i+2) = dFi(i, i+2, F);
      f[i] = -F;
     }

  {
    // double F = Fi(N);
    // M(N,N-1) = dFi(N, N-1, F);
    M(N,N) = 1; //dFi(N, N, F);
    f[N] = 0;
  }

  gmm::copy(M, M2);

  gmm::iteration iter(tol);
  //gmm::identity_matrix PR;
  gmm::ilutp_precond<gmm::csc_matrix<double> > PR(M2, 10, 1e-14);

  for (int i = 0; i < x_steps; ++i)
    u[i] = 0;

  gmm::bicgstab(M2, u, f, PR, iter);

  for (int i = 0; i < x_steps; ++i)
    Tn[i] += u[i];
}

void run()
{
  T.fill(273);
  T[0] = 1000;
  Tn = T;

  B.fill(0);

  double t = 0;
  int k = 0;
  while (t < 2)
    {
      int ki = 0;
      double err = PInf;
      do
        {
          Tprev = Tn;
          step_gmm(t);
          ++ki;

          err = norm(Tprev-Tn);
          if (ki % 100 == 0)
            std::cerr << ki << " " << err << "\n";
        }
      while (err > tol);

      for (int i = 0; i < x_steps; ++i)
        B[i] += dB(i, (T[i]+Tn[i])/2);

      T = Tn;

      // for (int i = 0; i < x_steps; ++i)
      //   std::cout << i*dx << " " << T(i) << " " << B(i) << "\n";
      // std::cout << "\n\n";

      t += dt;
      k += ki;

      // if (k % 1000 == 0)
        std::cerr << t << " " << k << "\n";
    }

  for (int i = 0; i < x_steps; ++i)
    std::cout << i*dx << " " << T(i) << " " << B(i) << "\n";
  std::cout << "\n\n";
}


int main()
{
  // dt = 1e-2;
  // while (dt > 1e-8)
  //   {
  //     run();
  //     dt /= 10;
  //   }

  dt = 1e-3;
  run();
  return 0;
}
