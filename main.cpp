#include <iostream>
#include <armadillo>

#include <math.h>

using std::cin;
using std::cout;
using std::cerr;

using namespace arma;

const double PInf = 1e25;
const double ZInf = 1e-25;

void shuttle(vec& a, vec& c, vec& b, double d, double e, vec& f, vec& u)
{
  const int N = a.n_elem;
  vec q(N-1);
  vec p(N-1);

  q[0] = f[0]/c[0];
  p[0] = -b[0]/c[0];
  const double r = -d/c[0];

  q[1] = (f[1] - a[1]*q[0]) / (c[1] + a[1]*p[0]);
  p[1] = -(b[1] + a[1]*r) / (c[1] + a[1]*p[0]);

  for (int i = 2; i < N-1; ++i)
    {
      q[i] = (f[i] - a[i]*q[i-1]) / (c[i] + a[i]*p[i-1]);
      p[i] = -b[i] /  (c[i] + a[i]*p[i-1]);
    }

  u[N-1] = (f[N-1] - e*q[N-3] - e*p[N-3]*q[N-2] - a[N-1]*q[N-2])
    / (e*p[N-3]*p[N-2] + a[N-1]*p[N-2] + c[N-1]);

  for (int i = N-2; i > 0; --i)
    {
      u[i] = q[i] + p[i]*u[i+1];
    }

  u[0] = q[0] + p[0]*u[1] + r*u[2];
}

const double len = 1;
const int x_steps = 800;
const double dx = len/x_steps;
const double V = dx*dx*dx;
const double Sx = dx*dx;
double dt = 1e-4;

vec::fixed<x_steps> T;
vec::fixed<x_steps> Tn;
vec::fixed<x_steps> Tprev;

vec::fixed<x_steps> B;


vec::fixed<x_steps> f;
vec::fixed<x_steps> a;
vec::fixed<x_steps> b;
vec::fixed<x_steps> c;
vec::fixed<x_steps> u;


double tol = 1e-8;

#include "params.h"

double sqr(double v)
{
  return v*v;
}

double sign(double v)
{
  return (v > 0 ? 1 : (v < 0 ? -1 : 0));
}

double dB(int x, double T)
{
  double K = 1e8*exp(-1e4/T);
  return (1-B(x))*(1-exp(-K*dt));
}


double Lam(int x, double Tc)
{
  double b = B[x] + dB(x,(T(x)+Tn(x))/2)/2;
  double lam1 = 10 + lamT2*sqr(Tc-273);
  double lam2 = 100 + lamT2*sqr(Tc-273);

  return 1.0 / (b/lam1 + (1-b)/lam2);
}

double Dx(int x, double Tc, double Tr)
{
  return Sx * 1.0 / (dx * 0.5 / Lam(x, Tc)
                     + (dx * 0.5 / Lam(x+1, Tr)));
}

double CRhoV(int x, double Tc)
{
  double b = B[x] + dB(x,(T(x)+Tn(x))/2)/2;
  double rho = 1700;
  double c1 = 100;
  double c2 = 1000 + cT*(Tc-273);
  return (b*c1 + (1-b)*c2)*rho*V;
}

double CRhoV_prev(int x)
{
  double b = B[x];
  double rho = 1700;
  double c1 = 100;
  double c2 = 1000 + cT*(T(x)-273);
  return (b*c1 + (1-b)*c2)*rho*V;
}


double QV(int x, double T)
{
  return Q*dB(x,T)*V;
}

void step(double t)
{
  f = T;
  const size_t N = x_steps-1;
  for (size_t i = 1; i < N; ++i)
    {
      a[i] = -Dx(i-1, Tn(i-1), Tn(i));
      c[i] = Dx(i-1, Tn(i-1), Tn(i)) +  Dx(i, Tn(i), Tn(i+1)) + CRhoV(i, Tn(i))/dt;
      b[i] = -Dx(i, Tn(i), Tn(i+1));
      f[i] = CRhoV(i, T(i))/dt * T(i) + QV(i, (T(i)+Tn(i))/2);

      //std::cerr << i << " " << a[i] << " " << c[i] << " " << b[i] << " " << f[i] << "\n";
     }

  if (t < 1)
    {
      c[0] = 1;
      b[0] = 0;
      f[0] = 1000;
    }
  else
    {
      c[0] = 1;
      b[0] = -1;
      f[0] = 0;
    }
  const double d = 0;

  const double e = 0;
  a[N] = -1;
  c[N] = 1;
  f[N] = 0;

  shuttle(a, c, b, d, e, f, u);
  Tn = u;
}



double F0(int i, double Tc, double Tr)
{
  return (Tr-Tc)/dx;
}

double Fi(int i, double Tl, double Tc, double Tr)
{
  double Tl2 = Tn(i-1);
  double Tc2 = Tn(i);
  double Tr2 = Tn(i+1);
  return
    (CRhoV(i, Tc)*Tc - CRhoV_prev(i)*T(i))/dt
    - ( Dx(i-1, Tl2, Tc2)*Tl - (Dx(i-1, Tl2, Tc2)+Dx(i, Tc2, Tr2))*Tc + Dx(i, Tc2, Tr2)*Tr )
    - QV(i, (T(i)+Tc)/2);
}


double dT = 1e-4;

void step_new(double t)
{
  const size_t N = x_steps-1;

  const double d = 0;
  if (t < 1)
    {
      c[0] = 1;
      b[0] = 0;
      f[0] = 0;
    }
  else
    {
      double F = F0(0, Tn(0), Tn(1));
      c[0] = (F0(0, Tn(0)+dT, Tn(1)) - F) / dT;
      b[0] = (F0(0, Tn(0), Tn(1)+dT) - F) / dT;
      f[0] = -F;
    }

  for (size_t i = 1; i < N; ++i)
    {
      double F = Fi(i, Tn(i-1), Tn(i), Tn(i+1));
      a[i] = (Fi(i, Tn(i-1)+dT, Tn(i), Tn(i+1)) - F) / dT;
      c[i] = (Fi(i, Tn(i-1), Tn(i)+dT, Tn(i+1)) - F) / dT;
      b[i] = (Fi(i, Tn(i-1), Tn(i), Tn(i+1)+dT) - F) / dT;
      f[i] = -F;
     }

  const double e = 0;
  a[N] = -1;
  c[N] = 1;
  f[N] = 0;

  shuttle(a, c, b, d, e, f, u);

  Tn += u;
}

void run()
{
  T.fill(273);
  T[0] = 1000;
  Tn = T;

  B.fill(0);

  double t = 0;
  int k = 0;
  while (t < 2)
    {
      int ki = 0;
      double err = PInf;
      do
        {
          Tprev = Tn;
          step_new(t);
          ++ki;

          err = norm(Tprev-Tn);
          if (ki % 100 == 0)
            std::cerr << ki << " " << err << "\n";
        }
      while (err > tol);

      for (int i = 0; i < x_steps; ++i)
        B[i] += dB(i, (T[i]+Tn[i])/2);

      T = Tn;

      // for (int i = 0; i < x_steps; ++i)
      //   std::cout << i*dx << " " << T(i) << " " << B(i) << "\n";
      // std::cout << "\n\n";

      t += dt;
      k += ki;

      // if (k % 1000 == 0)
        std::cerr << t << " " << k << "\n";
    }

  for (int i = 0; i < x_steps; ++i)
    std::cout << i*dx << " " << T(i) << " " << B(i) << "\n";
  std::cout << "\n\n";
}


int main()
{
  // dt = 1e-2;
  // while (dt > 1e-8)
  //   {
  //     run();
  //     dt /= 10;
  //   }

  dt = 1e-4;
  run();
  return 0;
}
